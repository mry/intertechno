/***************************************************************************
                           mainthread.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include "vdcaccess.h"
#include <pthread.h>
#include "utils/threadinterface.h"
#include "utils/modelobserver.h"
#include "utils/persistencyitf.h"

class MainThread :
  public ThreadInterface,
  public ModelObserver
{
public:
  MainThread(PersistencyItf* pItf);
  ~MainThread();
  VDCAccess* getAccess();

  bool setVDCDsuid(char* dsuid);
  bool setVDCName(char* name);

  void Update() override;

protected:
  void run() override;

private:
  void checkSaveData();

private:
  pthread_t m_thread;
  VDCAccess m_access;

  dsuid_t m_vdcDsuid;
  std::string m_vdcName;

  PersistencyItf* m_persistency;
  bool m_TriggerStorage;
  uint8_t m_writeDelay;
};

#endif // MAINTHREAD_H
