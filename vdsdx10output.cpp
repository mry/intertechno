/***************************************************************************
                           VDSDSX10Output.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ivdcaccess.h"
#include "utils/logger.h"
#include "X10Communication/ioqueue.h"
#include "vdsdx10output.h"

#include "utils/interfacehelper.h"

#include <string.h>
#include <stdlib.h>

/***********************************************************************//**
  @comment:  static initializer
***************************************************************************/
IOQueue* VDSDX10Output::m_queue = nullptr;
uint8_t* VDSDX10Output::m_icon = nullptr;
size_t VDSDX10Output::m_iconSize = 0;
std::string VDSDX10Output::m_iconName = "default";

/***********************************************************************//**
  @method :  VDSDX10Output
  @comment:  default constructor
***************************************************************************/
VDSDX10Output::VDSDX10Output() :
  m_apply{false},
  m_tempValue{0.0},
  m_scene{0},
  m_address1{0},
  m_address2{0},
  m_name{"output"},
  m_function{0},
  m_group{8}, // joker
  m_mode{1} // switch on/off
{
  initialize();
}

/***********************************************************************//**
  @method :  VDSDX10Output
  @comment:  constructor
***************************************************************************/
VDSDX10Output::VDSDX10Output(int32_t address1, int32_t address2) :
  m_apply{false},
  m_tempValue{0.0},
  m_scene{0},
  m_address1{address1},
  m_address2{address2},
  m_name{"output"},
  m_function{0},
  m_group{8}, // joker
  m_mode{1}  // switch on/off
{
  initialize();
}

/***********************************************************************//**
  @method :  initialize
  @comment:  called from constructors
***************************************************************************/
void VDSDX10Output::initialize()
{
  for (int i=0; i<MAX_SCENES; ++i) {
    m_value[i] = 0.0;
  }

  m_value[13] = 0.0;   // min scene
  m_value[14] = 100.0; // max scene
  m_value[40] = 0.0;   // Fade Off

  for (int i = 0; i < MAX_GROUPS; ++i) {
      m_groupMember[i] = false;
  }
  m_tempValue = 0.0;
}

/***********************************************************************//**
  @method :  ~VDSDX10Output
  @comment:  destructor
***************************************************************************/
VDSDX10Output::~VDSDX10Output()
{
}

/***********************************************************************//**
  @method :  setQueue
  @comment:  set queue
  @param  :  queue
***************************************************************************/
void VDSDX10Output::setQueue(IOQueue* queue)
{
  m_queue = queue;
}

/***********************************************************************//**
  @method :  setGlobalIcon
  @comment:  set Icon of device type VDSDX10Output
             image must be of type png 16
  @param  :  icon16png icon
  @param  :  size size of the icon
  @param  :  iconName name
***************************************************************************/
void VDSDX10Output::setGlobalIcon(const uint8_t* icon16png, size_t size, std::string &iconName)
{
  if (m_icon) {
    free (m_icon);
    m_icon = NULL;
  }
  m_icon = (uint8_t*)malloc(size);
  memcpy(m_icon, icon16png, size);
  m_iconSize = size;
  m_iconName = iconName;
}

/***********************************************************************//**
  @method :  getValue
  @comment:  getter
  @param  :  channel index of array
  @return :  channel value
***************************************************************************/
double VDSDX10Output::getValue(int channel) const
{
  if (channel == 0) {
    return m_tempValue;
  }
  LOG(LOG_ERR,"getValue:channel out of range:%i\n", channel);
  return -1;
}

/***********************************************************************//**
  @method :  getApply
  @comment:  getter
  @return :  apply value
***************************************************************************/
bool VDSDX10Output::getApply() const
{
  return m_apply;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::process()
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleSetControl(int32_t value, int32_t group, int32_t zone)
{
  // TODO
  LOG(LOG_INFO,"setControl:value:%i, group:%i, zone:%i\n", value, group, zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleSetChannelValue(bool apply, double value, int32_t channel)
{
  // TODO
  LOG(LOG_INFO,"handleSetChannelValue:apply: %i, value: %lf, channel: %i \n", apply, value, channel);
  if (0 == channel) {
    m_tempValue = value;
  }
  m_apply = apply;
  if (m_apply) {
    emitValue(m_tempValue);
  }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone)
{
  m_scene = scene;
  m_tempValue  = m_value[m_scene];
  LOG(LOG_DEBUG, "handleSetCallScene: scene:%i, force:%d, group:%i, zone:%i, value:%lf\n", scene, force, group, zone, m_tempValue);
  emitValue(m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleSetSaveScene(int32_t scene, int32_t group, int32_t zone)
{
  m_scene = scene;
  m_value[m_scene] = m_tempValue;
  LOG(LOG_DEBUG, "handleSetSaveScene: scene:%i, group:%i, zone:%i, value:%lf\n", scene, group, zone, m_value[m_scene]);
  emitValue(m_tempValue);
  notifyModelChange();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleSetMinScene(int32_t group, int32_t zone)
{
  m_scene = 0;
  m_tempValue = m_value[m_scene];
  LOG(LOG_DEBUG, "handleSetMinScene: group:%i, zone:%i, value:%lf\n", group, zone, m_tempValue);
  emitValue(m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleIdentify(int32_t /*group*/, int32_t /*zone_id*/)
{
  emitValue(0);
  emitValue(100);
  emitValue(0);
  emitValue(100);
  emitValue(m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetPrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_uint(property, name, 8); // joker
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  //(void)query; (void) name;

  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_add_bool(reply, "blink" ,      true);
  dsvdc_property_add_bool(reply, "dontcare",    true);
  dsvdc_property_add_bool(reply, "outmode",     true);
  dsvdc_property_add_bool(reply, "outvalue8",   true);
  dsvdc_property_add_bool(reply, "transt",      true);
  dsvdc_property_add_bool(reply, "jokerconfig", true);

  dsvdc_property_add_property(property, name, &reply);
};

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "1.0.1");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "1.0.0");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetConfigURL(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  static const size_t LEN = 20;
  char data[LEN];
  InterfaceHelper::GetPrimaryIp(data, LEN);
  std::string output = "http://" + std::string(data) + ":8090";
  dsvdc_property_add_string(property, name, output.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string guid = "guid:";
  dsuid_t dsuid = getDsuid();
  char data[DSUID_STR_LEN];
  dsuid_to_string(&dsuid,data);
  guid += data;
  dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string guid = "intertechno:";
  dsuid_t dsuid = getDsuid();
  char data[DSUID_STR_LEN];
  dsuid_to_string(&dsuid,data);
  guid += data;
  dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetModelUID(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsuid_t dsuid = getDsuid();
  char data[DSUID_STR_LEN];
  dsuid_to_string(&dsuid, data);
  dsvdc_property_add_string(property, name, data); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetName(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, m_name.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetModel(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string info("Output");
  dsvdc_property_add_string(property, name, info.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  //char dsuidstringVdcd[DSUID_STR_LEN];
  //dsuid_t temp = getDsuid();
  //::dsuid_to_string(&temp, dsuidstringVdcd);

  //char info[256];
  //strcpy(info, "dsuid:");
  //strcat(info, dsuidstringVdcd);
  dsvdc_property_add_string(property, name, "intertechno:binaryOutput");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_add_string(reply, "name",         "output");
  dsvdc_property_add_int(reply,    "defaultGroup", 1);        // light?
  dsvdc_property_add_uint(reply,    "function",     0);       // 0: on off only
  dsvdc_property_add_uint(reply,    "outputUsage",  0);       // 0: undefined
  dsvdc_property_add_bool(reply,   "variableRamp", false);    // no ramp
  dsvdc_property_add_string(reply, "type",         "output"); // really needed?

  LOG(LOG_DEBUG, "VDSDX10Output::handleGetOutputDescription\n");

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nGroup;
  if (dsvdc_property_new(&nGroup) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }

  dsvdc_property_add_uint(reply, "mode", m_mode);
  dsvdc_property_add_bool(reply, "pushChanges", false);

  LOG(LOG_DEBUG, "mode  %i\n", m_mode);
  
  // groups
  for (unsigned int i = 0; i < MAX_GROUPS; ++i) {
    if (m_groupMember[i]) {
      dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true); // light  //TODO
    } else {
      if (m_group == i) {
        dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true);
      }
    }
  }
  dsvdc_property_add_property(reply, "groups", &nGroup);

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }
  dsvdc_property_add_uint(nProp,     "channelIndex", 0);
  dsvdc_property_add_double(nProp,   "max",          100.0);
  dsvdc_property_add_double(nProp,   "min",          0.0);
  dsvdc_property_add_string(nProp,   "name",         "brightness");
  dsvdc_property_add_double(nProp,   "resolution" ,  1.0);
  dsvdc_property_add_property(reply, "0",            &nProp);
  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetChannelSettings(dsvdc_property_t * /*property*/, const dsvdc_property_t * /*query*/, char* /*name*/)
{
  /* nop: no channel settings defined */
  LOG(LOG_INFO, "handleGetChannelSettings\n");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDX10Output::handleGetChannelStates(dsvdc_property_t * property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }
  //dsvdc_property_add_double(nProp, "age", 0);
  dsvdc_property_add_double(nProp, "value", m_tempValue);

  dsvdc_property_add_property(reply, "0", &nProp);
  dsvdc_property_add_property(property, name, &reply);
  LOG(LOG_INFO, "Request channel value: %lf\n", m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t VDSDX10Output::handleSetButtonInputSettings(dsvdc_property_t */*property*/, const dsvdc_property_t * properties, char* name)
{
  dsvdc_property_t *buttonInputProp;
  dsvdc_property_get_property_by_name(properties, name, &buttonInputProp);

  dsvdc_property_t *indexProperty;
  dsvdc_property_get_property_by_name(buttonInputProp, "0", &indexProperty);

  for (size_t i = 0; i < dsvdc_property_get_num_properties(indexProperty); i++) {
    char* propName = NULL;
    if (DSVDC_OK != dsvdc_property_get_name(indexProperty, i, &propName)) {
      continue;
    }
    if (0 == strcmp(propName, "function")) {
      dsvdc_property_get_uint(indexProperty, i, &m_function);
    }
    else if (0 == strcmp(propName, "group")) {
      dsvdc_property_get_uint(indexProperty, i, &m_group);
    }
  }
  notifyModelChange();
  return DSVDC_OK;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t VDSDX10Output::handleSetOutputSettings(dsvdc_property_t */*property*/, const dsvdc_property_t * properties, char* name)
{
  dsvdc_property_t *outputSettings;
  dsvdc_property_get_property_by_name(properties, name, &outputSettings);

  dsvdc_property_t *modeProperty;

  int retVal = dsvdc_property_get_property_by_name(outputSettings, "mode", &modeProperty);
  if (DSVDC_OK == retVal) {
    dsvdc_property_get_uint(outputSettings, 0, &m_mode);
  }

  dsvdc_property_t *groupsProperty;
  int groupIndx = 0;
  bool groupSet = false;
  retVal = dsvdc_property_get_property_by_name(outputSettings, "groups", &groupsProperty);
  if (DSVDC_OK == retVal) {
    for (size_t i = 0; i < dsvdc_property_get_num_properties(groupsProperty); i++) {

      char* propName = NULL;
      if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
        continue;
      }
      groupIndx = std::stoi(propName);

      if (groupIndx >= MAX_GROUPS) {
        continue;
      }

      if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
        continue;
      }

      if (DSVDC_OK == dsvdc_property_get_bool(groupsProperty, i, &groupSet))
      {
          m_groupMember[groupIndx] = groupSet;
      }
    }
  }
  LOG(LOG_INFO, "VDSDX10Output::handleSetOutputSettings mode:%i group:%i value:%d\n", m_mode, groupIndx, groupSet);
  notifyModelChange();
  return retVal;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t VDSDX10Output::handleSetName(dsvdc_property_t */*property*/, const dsvdc_property_t * properties, char* /*name*/)
{
  char* data;
  dsvdc_property_get_string(properties,0,&data);
  m_name = data;
  notifyModelChange();
  return DSVDC_OK;
}

/***********************************************************************//**
  @method :  emitValue
  @comment:  set value to x10 device
  @param  :  value
***************************************************************************/
void VDSDX10Output::emitValue(double value)
{
  if (m_queue) {
    auto data = std::shared_ptr <TransferData> (new TransferData());
    data->m_address1 = m_address1;
    data->m_address2 = m_address2;
    data->m_on = (value > 50);
    m_queue->insert(data);
  }
}

/***********************************************************************//**
  @method :  handleDeviceIcon
  @comment:  handler for device icon
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void VDSDX10Output::handleDeviceIcon(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "VDSDX10Output::handleDeviceIcon\n");
  dsvdc_property_add_bytes(property, name, m_icon, m_iconSize);
}

/***********************************************************************//**
  @method :  handleDeviceIconName
  @comment:  handler for device icon name
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void VDSDX10Output::handleDeviceIconName(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "VDSDX10Output::handleDeviceIconName\n");
  dsvdc_property_add_string(property, name, m_iconName.c_str());
}
