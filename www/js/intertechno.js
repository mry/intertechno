/* ds-intertechno

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

var isTouch = Modernizr.touch ? 1 : 0;
var selectedDevice;

/*------------------------------------------------------------------------------------------------*/

function resolveAddress1(index) {
    var items =  ["A", "B", "C","D", "E", "F","G", "H", "I","J", "K", "L","M", "N", "O", "P"];
    return items[index];
}

function getAllDevices() {
    $.getJSON('/devicesOutputList',
        {
        }
        , function (jd) {
            $("#device-list > tbody").empty();
            for (var index in jd.devices) {
                var item = jd.devices[index];

                var element = (

                    "<tr class=\"rows\" data-href=\"" + item.dsuid + "\">"

                    + "<td>"
                    + item.name
                    + "</td>"

                    + "<td>"
                    + item.dsuid
                    + "</td>"

                    + "<td>"
                    + resolveAddress1(item.address1)
                    + "</td>"

                    + "<td>"
                    + (item.address2+1)
                    + "</td>"

                    + "</tr>"
                );
                
                $('#device-list').append(element);
            };
        });
}

/*------------------------------------------------------------------------------------------------*/
var app = $.sammy(function () {

    function runBrowse() {
        $('#panel-heading').text("intertechno client");
        $('#breadcrump').addClass('hide');
        $('#salamisandwich').removeClass('hide').find("tr:gt(0)").remove();
        $('#mainpanel').removeClass('hide');

        $('#removedialog').modal('hide');
        $('#adddialog').modal('hide');

        getAllDevices();
    }

    runBrowse();
});

$(document).ready(function() {

    $('#device-list').delegate("tr.rows", "click", function(){
        selectedDevice = $(this).data("href");
        $('#removedialog').modal('show');        
    });

    $('#removedialog').on('shown.bs.modal', function () {
         $('#deviceid').val(selectedDevice);
    });

});

function removeDevice() {
    var formData = JSON.stringify({ "dsuid" : selectedDevice });
    $.post("removeOutput", formData , "json");
    $('#removedialog').modal('hide');
    getAllDevices();
}

function addDevice() {
 
    var e1 = document.getElementById("address1");
    var address1 = parseInt(e1.options[e1.selectedIndex].value);

    var e2 = document.getElementById("address2");
    var address2 = parseInt(e2.options[e2.selectedIndex].value);
 
    var formData = JSON.stringify({ "address1" : address1, "address2" : address2  });
    
    $.post("addOutput", formData , "json");
    
    $('#adddialog').modal('hide');
    
    getAllDevices();
}

function addDeviceDialog() {
    $('#adddialog').modal('show'); 
}