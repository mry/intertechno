/***************************************************************************
                           factory.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "factory.h"
#include "ivdsd.h"
#include "vdsdx10output.h"
#include "utils/logger.h"

#include <iostream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/foreach.hpp>

#include <boost/serialization/export.hpp>



BOOST_CLASS_EXPORT_GUID(VDSDX10Output, "output");

/***********************************************************************//**
  @method : Factory
  @comment: constructor
  @param  : port serial port by string (e.g. /dev/ttyUSB0)
  @param  : dataFile persistency file
  @param  : data directory for data
***************************************************************************/
Factory::Factory(std::string& port, std::string& dataFile, std::string& dataDirectory) :
  m_bStartedUp{false},
  m_serialPort{port},
  m_persistentFile{dataFile},
  m_dataDirectory{dataDirectory}
{
  std::string filename = m_dataDirectory + "/vdsd_yellow.png";
  try {
    std::ifstream file;
    file.open (filename, std::ios::in|std::ios::binary|std::ios::ate);
    m_size = file.tellg();
    m_memblock = new uint8_t [m_size];
    file.seekg (0, std::ios::beg);
    file.read ((char*)m_memblock, m_size);
    file.close();

    std::string defaultName = "VDSDX10Output";
    VDSDX10Output::setGlobalIcon(m_memblock, m_size, defaultName);
  
  } catch (std::bad_alloc& e) {
    LOG(LOG_CRIT, "can not load image file %s. does not exist:",filename.c_str(), e.what());
  }

  IVDC::setModelUpdater(&m_updateSubject);
  IVDSD::setModelUpdater(&m_updateSubject);
}

/***********************************************************************//**
  @method : ~Factory
  @comment: destructor
***************************************************************************/
Factory::~Factory()
{
  delete [] m_memblock;
}

/***********************************************************************//**
  @method : startup
  @comment: startup system
***************************************************************************/
void Factory::startup()
{
  if (!m_bStartedUp) {

    // Setup command transfer queue
    m_outputQueue = std::make_shared<IOQueue>();
    VDSDX10Output::setQueue(m_outputQueue.get());

    // Setup serial line
    std::shared_ptr <SerialInterface> local = std::make_shared<SerialLine>(m_serialPort.c_str());
    m_serialInterface = local;

    // setup port
    m_serialInterface->openPort();
    m_serialInterface->setBaud(38400);
    m_serialInterface->setDtr(false);
    m_serialInterface->setRts(false);

    // Setup sender thread
    m_x10Sender = std::make_shared<X10SenderThread> (m_outputQueue, m_serialInterface);

    // Setup Main Thread
    m_mainThread = std::make_shared<MainThread> (this); // shared from this

    std::string documentRoot =  m_dataDirectory + "/www";
    m_webserver = std::make_shared<Webserver> (this, documentRoot); // shared from this

    m_updateSubject.attach(m_mainThread.get());

    // Set VdcAccess
    IVDSD::registerVdcAcces(m_mainThread->getAccess());

    // Load previous setup
    loadData();

    // check if vdc exist, else create one.
    checkCreateVdc();

    // start threads
    m_mainThread->startThread();
    m_x10Sender->startThread();
    m_webserver->startThread();

    m_bStartedUp = true;
  }
}

/***********************************************************************//**
  @method : createX10OutputDevice
  @comment: create a device with random dsuid and it addresses
  @param  : address1 address part 1
  @param  : address2 address part 2
***************************************************************************/
void Factory::createX10OutputDevice(int32_t address1, int32_t address2)
{
  std::unique_lock<std::mutex> lock(m_CreationMutex);
  boost::shared_ptr<VDSDX10Output> myVdsd(new VDSDX10Output(address1, address2));
  dsuid_t vdcdDsuid;
  dsuid_generate_v4_random(&vdcdDsuid);
  myVdsd->setDsuid(vdcdDsuid);
  m_mainThread->getAccess()->registerVDSD(myVdsd);
}

/***********************************************************************//**
  @method : removeX10OutputDevice
  @comment: remove x10 output device
  @param  : dsuid dsuid of vdcd
***************************************************************************/
void Factory::removeX10OutputDevice(dsuid_t& dsuid)
{
  std::unique_lock<std::mutex> lock(m_CreationMutex);
  m_mainThread->getAccess()->unregisterVDSD(dsuid);
}

/***********************************************************************//**
  @method : getDevices
  @comment: get vector of ivdcd
  @return : vector of ivdsd
***************************************************************************/
std::vector<boost::shared_ptr<IVDSD> > Factory::getDevices()
{
  std::unique_lock<std::mutex> lock(m_CreationMutex);
  return m_mainThread->getAccess()->getDevices();
}

/***********************************************************************//**
  @method : shutdown
  @comment: shutdown system
***************************************************************************/
void Factory::shutdown()
{
  if (m_bStartedUp) {
    // stop threads
    m_mainThread->stopThread();
    m_x10Sender->stopThread();

    // delete objects
    m_mainThread.reset();
    m_x10Sender.reset();

    m_outputQueue.reset();
    m_serialInterface.reset();

    m_webserver.reset();

    m_bStartedUp = false;
  }
}

/***********************************************************************//**
  @method : checkCreateVdc
  @comment: check if vdc exist, if not create one
***************************************************************************/
void Factory::checkCreateVdc()
{
  if (!m_mainThread->getAccess()->getVDC()) {
      boost::shared_ptr<VDC> myVdc(new VDC());

      dsuid_t vdcDsuid;
      dsuid_generate_v4_random(&vdcDsuid);
      myVdc->setDsuid(vdcDsuid);

      std::string vdcName = "Intertechno vDC";
      myVdc->setName(vdcName);
      m_mainThread->getAccess()->registerVDC(myVdc);
  }
}

/***********************************************************************//**
  @method : loadData
  @comment: load data from file
***************************************************************************/
void Factory::loadData()
{
  std::unique_lock<std::mutex> lock(m_CreationMutex);
  // load data
  // create and open an archive for input  
  std::string persistentData =m_dataDirectory + "/" + m_persistentFile;

  std::ifstream ifs(persistentData);
  if (!ifs.good()) {
    LOG(LOG_CRIT, "can not load data. File %s does not exist", persistentData.c_str());
    return;
  }
  boost::archive::text_iarchive ia(ifs);
  // read class state from archive
  VDCAccess* access = m_mainThread->getAccess();
  if (access) {
    ia >> *access;
  } else {
    LOG(LOG_CRIT, "can not load data");
  }
  // archive and stream closed when destructors are called
}

/***********************************************************************//**
  @method : saveData
  @comment: save data to file
***************************************************************************/
void Factory::saveData()
{
  std::unique_lock<std::mutex> lock(m_CreationMutex);
  // create and open a character archive for output

  std::string persistentData =m_dataDirectory + "/" + m_persistentFile;
  std::ofstream ofs(persistentData);
  boost::archive::text_oarchive oa(ofs);
  // write class instance to archive
  VDCAccess* access = m_mainThread->getAccess();
  if (access) {
    oa << *access;
  } else {
    LOG(LOG_CRIT, "can not save data\n");
  }
  // archive and stream closed when destructors are called
}
