/***************************************************************************
                           mainthread.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/logger.h"
#include "mainthread.h"

#include "vdc.h"

static const uint8_t WRITE_DELAY = 10;

/***********************************************************************//**
  @method :  MainThread
  @comment:  constructor
***************************************************************************/
MainThread::MainThread(PersistencyItf* pItf):
  m_vdcName{"NotSet"},
  m_persistency{pItf},
  m_TriggerStorage{false},
  m_writeDelay{WRITE_DELAY}
{
}

/***********************************************************************//**
  @method :  ~MainThread
  @comment:  destructor. stop the thread if still running
***************************************************************************/
MainThread::~MainThread()
{
  stopThread();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MainThread::Update()
{
  LOG(LOG_DEBUG,"MainThread::Update called\n");
  m_TriggerStorage = true;
  // reset wait delay
  m_writeDelay = WRITE_DELAY;
}

/***********************************************************************//**
  @method :  getAccess
  @comment:  get pointer to VDCACCess
  @return:   pointer to VDCAccess
***************************************************************************/
VDCAccess* MainThread::getAccess()
{
  return &m_access;
}

/***********************************************************************//**
  @method :  setVDCDsuid
  @comment:  set dsuid of vdc. Only allowed when not running
  @param  :  dsuid string representation of dsuid
  @return :  true successfully set
***************************************************************************/
bool MainThread::setVDCDsuid(char* dsuid)
{
  if (!isRunning()) {
    dsuid_from_string(dsuid, &m_vdcDsuid);
    return true;
  }
  return false;
}

/***********************************************************************//**
  @method :  setVDCName
  @comment:  set name of vdc. Only allowed when not running
  @param  :  name of vdc
  @return :  true sucessfully set
***************************************************************************/
bool MainThread::setVDCName(char* name)
{
  if (!isRunning()) {
    m_vdcName = name;
    return true;
  }
  return false;
}


/***********************************************************************//**
  @method :  checkSaveData
  @comment:  The function checks if saving the data is needed.
             It also delays writing the data. (Reduction of write cycles)
***************************************************************************/
void MainThread::checkSaveData()
{
  if (m_TriggerStorage) {
    if (m_writeDelay > 0) {
      --m_writeDelay;
    } else {
       m_writeDelay = WRITE_DELAY;
      LOG(LOG_DEBUG,"MainThread::Process Saving data to file\n");
      m_TriggerStorage = false;
      if (m_persistency) {
        m_persistency->saveData();
      }
    }
  }
}

/***********************************************************************//**
  @method :  run
  @comment:  The run function of the MainThread.
             It is called from the static function doRun.
***************************************************************************/
void MainThread::run()
{
  // setup vdc
  m_access.initializeLib();
  m_access.registerCallbacks();

  while(isRunning()) {
    m_access.run();
    checkSaveData();
  }

  m_access.unregisterCallbacks();
  m_access.shutdown();
}
