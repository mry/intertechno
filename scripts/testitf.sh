#!/bin/sh

## call this script with /testitf.sh <ip-address>
##./testitf.sh 192.168.0.15

if [ $# -ne 1 ]; then
	echo required parameters IP address
	exit 1
fi 

IP=$1

echo "IP address: $IP"


curl_post="curl   -sX POST  --header 'Content-Type: application/json' --header 'Accept: application/json'"
curl_get="curl    -sX GET   --header 'Content-Type: application/json' --header 'Accept: application/json'"
curl_patch="curl  -sX PATCH --header 'Content-Type: application/json' --header 'Accept: application/json'"


echo " "
echo "-1. dump sensor data to check whether it is connected"
$curl_post http://${IP}:8090/addOutput -d "{ \"address1\": 1, \"address2\": 2 }"
# $curl_post http://${IP}:8090/addOutput -d "{ \"address1\": 1, \"address2\": }"

sleep 2

echo " "
echo "1. reset measure mode"
$curl_get http://${IP}:8090/devicesOutputList


echo " "
echo "3. remove device"
$curl_post http://${IP}:8090/removeOutput -d "{ \"dsuid\": \"cf0a47143612449d9188ee4a86d6622300\"}"
$curl_post http://${IP}:8090/removeOutput -d "{ \"dsuid\": \"7be895e890bc4b4b95604ed2773ffc5400\"}"


