/***************************************************************************
                           vdcaccess.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDCAccess_H
#define VDCAccess_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/list.hpp>
#include <boost/noncopyable.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <stdlib.h>
#include <map>
#include <string.h>
#include <stdio.h>
#include <mutex>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

#include "utils/dsuidutils.h"
#include "ivdcaccess.h"
#include "ivdc.h"
#include "vdc.h"
#include "ivdsd.h"
#include "vdsdx10output.h"
#include "utils/logger.h"

class VDCAccess :
  public IVDCAccess,
  public boost::noncopyable
{
  //---------------------------------------------------------------------------
  friend class boost::serialization::access;

  template<class Archive>
  void save(Archive & ar, const unsigned int /*version*/) const
  {
    ar.template register_type<VDSDX10Output>();
    ar.template register_type<VDC>();
    try {
      ar & m_vdc;
      ar & m_vdsd;
    } catch (std::exception& ex) {
      LOG(LOG_ERR, ex.what());
    }
    LOG(LOG_INFO, "save data succcess\n");
  }

  template<class Archive>
  void load(Archive & ar, const unsigned int /*version*/)
  {
    ar.template register_type<VDSDX10Output>();
    ar.template register_type<VDC>();
    try {
      ar & m_vdc;
      ar & m_vdsd;
      LOG(LOG_INFO, "number of devices%i\n", m_vdsd.size());
    } catch (std::exception& ex) {
      LOG(LOG_ERR, "Exception: %s \n", ex.what());
    }
    LOG(LOG_INFO, "load data. Number of devices: %i\n", m_vdsd.size());
  }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

  //---------------------------------------------------------------------------

public:
  VDCAccess();
  bool initializeLib();
  void registerCallbacks();
  void unregisterCallbacks();

  void newSession(dsvdc_t* handle);
  void ping(dsvdc_t* handle , const char* dsuid);
  void endSession(dsvdc_t* handle);
  void setidentify(dsvdc_t *handle, char **dsuid, size_t n_dsuid, int32_t group, int32_t zone_id);
  void getprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* query);
  void setprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* properties);
  void setcontrol(dsvdc_t* handle, const char* dsuid, int32_t value, int32_t group, int32_t zone);
  void setChannelValue(dsvdc_t* handle, const char* dsuid, bool apply, double value, int32_t channel);
  void setCallScene(dsvdc_t* handle, const char* dsuid , int32_t scene, bool force, int32_t group, int32_t zone_id);
  void setSaveScene(dsvdc_t* handle, const char*  dsuid, int32_t scene, int32_t  group, int32_t  zone_id);
  void setMinScene(dsvdc_t* handle, const char*  dsuid, int32_t group, int32_t zone_id);

public:
  void registerVDC(boost::shared_ptr<IVDC> vdsm);
  boost::shared_ptr<IVDC> getVDC() override;

  void registerVDSD(boost::shared_ptr<IVDSD> vdc);
  void unregisterVDSD(dsuid_t& vdsdDsuid);

  std::vector<boost::shared_ptr<IVDSD> > getDevices();


  dsvdc_t* getHandle() const override;

  void run();
  void shutdown();

private:
  void resetConnectionState();

  void processAnnouncement();

private:
  dsvdc_t* m_handle;
  bool m_ready;

  typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> > VDSD_COLLECTION;
  typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> >::iterator VDSD_ITERATOR;
  typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> >::const_iterator VDSD_CONST_ITERATOR;

  VDSD_COLLECTION m_vdsd;

  std::mutex  m_AccessMutex;
  boost::shared_ptr<IVDC> m_vdc;
};

#endif // VDCAccess_H
