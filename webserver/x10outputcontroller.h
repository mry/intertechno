/***************************************************************************
                           x10outputcontroller.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef X10OUTPUTCONTROLLER_H
#define X10OUTPUTCONTROLLER_H

#include <mongoose/WebController.h>

class Webserver;

class X10OutputController :
  public Mongoose::WebController
{
  public:
  X10OutputController(Webserver* pController);
  void addOutputDevice(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void removeDevice(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void deviceList(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void setup();

  private:
  void fillResponse(Mongoose::StreamResponse &response, const std::string& status);
private:
  Webserver* m_pWebserver;
};

#endif // X10OUTPUTCONTROLLER_H