/***************************************************************************
                           x10outputcontroller.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "x10outputcontroller.h"

#include "webserver.h"
#include "vdsdx10output.h"
#include "utils/deviceadministrationitf.h"
#include "utils/converter.h"
#include "utils/logger.h"

#include <digitalSTROM/dsuid.h>

#include <boost/foreach.hpp>

// RapidJSON
#define RAPIDJSON_HAS_STDSTRING 1
#include "rapidjson/document.h"
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/writer.h>
#include <rapidjson/reader.h>

using namespace Mongoose;
using namespace std;

namespace
{
  static const std::string SUCCESS {"success"};
  static const std::string FAIL {"failed"};
}

/***********************************************************************//**
  @method :  X10OutputController
  @comment:  constructor
  @param  :  pController
***************************************************************************/
X10OutputController::X10OutputController(Webserver* pController) :
  m_pWebserver(pController)
{
}

void X10OutputController::fillResponse(Mongoose::StreamResponse &response, const std::string& status)
{
    using namespace rapidjson;
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("status");
    writer.String(status);
    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method :  addOutputDevice
  @comment:  handler to add a device
             e.g. http://127.0.0.1:8090/addOutput -d "{\"address1\":2, \"address2=\:1 }"
  @param  :  request
  @param  :  response
***************************************************************************/
void X10OutputController::addOutputDevice(Request &request, StreamResponse &response)
{
  try {
    std::string data  = request.getData();
    LOG(LOG_DEBUG, "X10OutputController::addOutputDevice %s \n", data.c_str());
    
    using namespace rapidjson;
    Document document;
    if (document.Parse<0>(data.c_str()).HasParseError()) {
      fillResponse(response, FAIL);
      return;
    }

    auto address1{document["address1"].GetInt()};
    auto address2{document["address2"].GetInt()};

    LOG(LOG_DEBUG, "X10OutputController::addOutputDevice %i, %i \n", address1, address2);

    if ((address1 >= 0) && (address2 >= 0)) {
      m_pWebserver->getAdminItf()->createX10OutputDevice(address1, address2);
    }
    fillResponse(response, SUCCESS);
  } catch (...) {
    fillResponse(response, FAIL);
  }
}

/***********************************************************************//**
  @method :  removeDevice
  @comment:  handler to remove a device
             e.g. http://127.0.0.1:8090/removeOutput -d "{\"dsuid\" : \"7d7791d8f00c47a988c35c3e22b4871e00\" }"
  @param  :  request
  @param  :  response
***************************************************************************/
void X10OutputController::removeDevice(Request &request, StreamResponse &response)
{
  try {
    std::string data  = request.getData();
    LOG(LOG_DEBUG, "X10OutputController::removeDevice %s \n", data.c_str());
    
    using namespace rapidjson;
    Document document;
    if (document.Parse<0>(data.c_str()).HasParseError()) {
      fillResponse(response, FAIL);
      return;
    }

    auto address {document["dsuid"].GetString()};

    dsuid_t tempDsuid;
    if (dsuid_from_string(address, &tempDsuid) == 0) {
      m_pWebserver->getAdminItf()->removeX10OutputDevice(tempDsuid);
    }
    fillResponse(response, SUCCESS);
  } catch (...) {
    fillResponse(response, FAIL);
  }
}

/***********************************************************************//**
  @method :  deviceList
  @comment:  handler to create a list with devices
             e.g. http://127.0.0.1:8080/devicesOutputList
  @param  :  request
  @param  :  response
***************************************************************************/
void X10OutputController::deviceList(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
  std::vector<boost::shared_ptr<IVDSD> > devices =
    m_pWebserver->getAdminItf()->getDevices();

  using namespace rapidjson;
  StringBuffer s;
  Writer<StringBuffer> writer(s);

  writer.StartObject();
  writer.Key("devices");

  writer.StartArray();

  BOOST_FOREACH (boost::shared_ptr<IVDSD> ivdsd, devices) {
    boost::shared_ptr<VDSDX10Output> pOutput = boost::dynamic_pointer_cast<VDSDX10Output> (ivdsd);
    if (!pOutput) {
      continue;
    }

    DSUID_STR(strDsuid);
    dsuid_t temp = pOutput->getDsuid();
    dsuid_to_string(&temp, strDsuid);

    // todo!!!!
    auto address1 = pOutput->getAddress1();
    auto address2 = pOutput->getAddress2();
    std::string  name = pOutput->getName();

    writer.StartObject();

    writer.Key("name");
    writer.String(name);

    writer.Key("dsuid");
    writer.String(strDsuid);

    writer.Key("address1");
    writer.Int(address1);

    writer.Key("address2");
    writer.Int(address2);

    writer.EndObject();
  }

  writer.EndArray();
  writer.EndObject();

  response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method :  setup
  @comment:  set up handlers
***************************************************************************/
void X10OutputController::setup()
{
  addRoute("POST", "/addOutput",              X10OutputController,  addOutputDevice);
  addRoute("POST", "/removeOutput",           X10OutputController,  removeDevice);
  addRoute("GET", "/devicesOutputList",       X10OutputController,  deviceList);
}
