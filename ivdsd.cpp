/***************************************************************************
                           ivdsd.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <string.h> // memcpy
#include <assert.h>

#include "ivdsd.h"
#include "ivdc.h"
#include "utils/logger.h"
#include "ivdcaccess.h"

namespace //anonymous namespace
{

static const int ANNOUNCE_DELAY = 7;

/***********************************************************************//**
  @method :  announce_device_cb
  @comment:  announce callback.
             This is registered with dsvdc_announce_device.
             It is called, when the device was accepted after
             registering it with dsvdc_announce_device
  @param  :  handle
  @param  :  code
  @param  :  arg the this pointer (must be correctly registered with dsvdc_announce_device)
  @param  :  userdata ?
***************************************************************************/
void announce_device_cb(dsvdc_t *handle, int code, void *arg, void *userdata)
{
  LOG(LOG_INFO, "IVDSD::device ANNOUNCED Callback\n");
  IVDSD* ivdsd = reinterpret_cast<IVDSD*>(arg);
  ivdsd->announceDevice(handle, code, userdata);
}

}; //anonymous namespace

/***********************************************************************//**
  @comment:  static initializer
***************************************************************************/
IVDCAccess* IVDSD::m_VDCAccess = nullptr;
ModelSubject* IVDSD::m_sModelUpdater = nullptr;

/***********************************************************************//**
  @method :  IVDSD
  @comment:  contructor
***************************************************************************/
IVDSD::IVDSD() :
  m_icon{nullptr},
  m_iconSize{0},
  m_conState{CON_OFFLINE},
  m_conState_old{CON_OFFLINE},
  m_trigAnnounce{false},
  m_zoneID{0}
{
}

/***********************************************************************//**
  @method :  ~IVDSD
  @comment:  destructor
***************************************************************************/
IVDSD::~IVDSD()
{
  free (m_icon);
}

/***********************************************************************//**
  @method :  modelUpdate
  @comment:  update model. Can be callled from outside
             when device is created or deleted
***************************************************************************/
void IVDSD::modelUpdate()
{
  notifyModelChange();
}

/***********************************************************************//**
  @method :  setDsuid
  @comment:  set dsuid of device
  @param  :  dsuid input
***************************************************************************/
void IVDSD::setDsuid(const dsuid_t& dsuid)
{
  m_dsuid = dsuid;
  notifyModelChange();
}

/***********************************************************************//**
  @method :  getDsuid
  @comment:  get dsuid of device
  @return :  dsuid
***************************************************************************/
dsuid_t IVDSD::getDsuid() const
{
  return m_dsuid;
}

/***********************************************************************//**
  @method :  setIcon
  @comment:  set Icon of device.
             image must be of type png 16
  @param  :  icon16png icon
  @param  :  size size of the icon
  @param  :  iconName name
***************************************************************************/
void IVDSD::setIcon(const uint8_t* icon16png, size_t size, std::string &iconName)
{
  if (m_icon) {
    free (m_icon);
    m_icon = nullptr;
  }

  m_icon = (uint8_t*)malloc(size);
  memcpy(m_icon, icon16png, size);
  m_iconSize = size;
  m_iconName = iconName;
}

/***********************************************************************//**
  @method :  checkAnnounce
  @comment:  check announce state of device.
             called cyclically.
***************************************************************************/
void IVDSD::checkAnnounce()
{
  assert(m_VDCAccess);

  char dsuidstringVdcd[DSUID_STR_LEN];
  ::dsuid_to_string(&m_dsuid, dsuidstringVdcd);

  if (m_conState == CON_OFFLINE) {

    if (m_trigAnnounce) {
      m_trigAnnounce = false;

      char dsuidstringContainer[DSUID_STR_LEN];
      dsuid_t vdc_dsuid = m_VDCAccess->getVDC()->getDsuid();
      ::dsuid_to_string(&vdc_dsuid, dsuidstringContainer);

      if (dsvdc_announce_device(m_VDCAccess->getHandle(), dsuidstringContainer, dsuidstringVdcd, this, announce_device_cb) == DSVDC_OK) {
        if (m_conState_old != m_conState) {
          m_conState_old = m_conState;
          LOG(LOG_DEBUG, "IVDSD::device OFFLINE: %s\n", dsuidstringVdcd);
        }
        m_conState = CON_ANNOUNCING;
      }
    }
  }else if (m_conState == CON_ANNOUNCING) {
    LOG(LOG_DEBUG, "IVDSD::device ANNOUNCING:%s \n", dsuidstringVdcd);

    time_t now = time(NULL);
    if ((now-ANNOUNCE_DELAY) > m_announcingStart) {
      if (m_conState_old != m_conState) {
         m_conState_old = m_conState;
        LOG(LOG_WARNING, "IDVDSD:: device ANNOUNCING timeout -> falling back to OFFLINE: %s\n", dsuidstringVdcd);
      }
      m_conState = CON_OFFLINE;
    }


  } else {
    char dsuidstringVdcd[DSUID_STR_LEN];
    ::dsuid_to_string(&m_dsuid, dsuidstringVdcd);
    if (m_conState_old != m_conState) {
      m_conState_old = m_conState;
      LOG(LOG_DEBUG, "IVDSD::device ANNOUNCED: %s\n", dsuidstringVdcd);
    }
    process();
  }
}

/***********************************************************************//**
  @method :  announceDevice
  @comment:  callback after registration
  @param  :  handle
  @param  :  code
  @param  :  arg
***************************************************************************/

void IVDSD::announceDevice(dsvdc_t * /*handle*/, int code, void */*userdata*/)
{
  if ((CON_ANNOUNCING == m_conState) && (0 == code)) {
    m_conState = CON_ANNOUNCED;
    LOG(LOG_INFO, "IVDSD::device ANNOUNCED CB code:%i\n", code);
  } else {
    LOG(LOG_WARNING, "IVDSD::device ANNOUNCED CB  failed code:%i\n", code);
  }
}

/***********************************************************************//**
  @method :  sigOffline
  @comment:  reset announced state to offline
***************************************************************************/
void IVDSD::sigOffline()
{
  LOG(LOG_DEBUG, "IVDSD::sigOffline\n");
  m_conState = CON_OFFLINE;
}

/***********************************************************************//**
  @method :  sigAnnounce
  @comment:  start announce operation
***************************************************************************/
void IVDSD::sigAnnounce()
{
  LOG(LOG_DEBUG, "IVDSD::sigAnnounce\n");
  m_trigAnnounce = true;
  m_announcingStart = time(NULL);
}

/***********************************************************************//**
  @method :  getConnectionState
  @comment:  get the connection state
  @param  :  state
***************************************************************************/
IVDSD::CONNECTION_STATE IVDSD::getConnectionState() const
{
  return m_conState;
}

/***********************************************************************//**
  @method :  registerVdcAccess
  @comment:  register VDC container
  @param  :  ivdc input
***************************************************************************/
void IVDSD::registerVdcAcces(IVDCAccess* ivdc)
{
  m_VDCAccess = ivdc;
}

/***********************************************************************//**
  @method :  getVdcAccess
  @comment:  get the vdc container
  @return :  container
***************************************************************************/
IVDCAccess* IVDSD::getVdcAccess() const
{
  assert(m_VDCAccess);
  return m_VDCAccess;
}

/***********************************************************************//**
  @method :  handleGetVDSDProperties
  @comment:  handle a property query.
             Overwrite the handle... in your own class, if needed.
  @param  :  handle handle of container
  @param  :  property property to fill data
  @param  :  query property query
***************************************************************************/
void IVDSD::handleGetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * query)
{
  char *name;

  for (size_t i = 0; i < dsvdc_property_get_num_properties(query); i++) {
    int ret = dsvdc_property_get_name(query, i, &name);
    if (ret != DSVDC_OK) {
      return;
    }

    if (!name)
    {
      LOG(LOG_DEBUG, "IVDSD:: handleGetVDSDProperties not yet handling wildcard properties\n");
      continue;
    }

    LOG(LOG_DEBUG, "**** request name: %s\n", name);
    if (0 == strcmp(name, "primaryGroup"))
    {
      handleGetPrimaryGroup(property, query, name);
    }
    else if (0 == strcmp(name, "modelFeatures"))
    {
      handleGetModelFeatures(property, query, name);
    }
    else if (0 == strcmp(name, "buttonInputDescriptions"))
    {
      handleGetButtonInputDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "buttonInputSettings"))
    {
      handleGetButtonInputSettings(property, query, name);
    }
    else if (0 == strcmp(name, "outputDescription"))
    {
      handleGetOutputDescription(property, query, name);
    }
    else if (0 == strcmp(name, "outputSettings"))
    {
      handleGetOutputSettings(property, query, name);
    }
    else if (0 == strcmp(name, "channelDescriptions"))
    {
      handleGetChannelDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "channelSettings"))
    {
      handleGetChannelSettings(property, query, name);
    }
    else if (0 == strcmp(name, "channelStates"))
    {
      handleGetChannelStates(property, query, name);
    }
    else if (0 == strcmp(name, "binaryInputDescriptions"))
    {
      handleGetBinaryInputDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "binaryInputSettings"))
    {
      handleGetBinaryInputSettings(property, query, name);
    }
    else if (0 == strcmp(name, "sensorDescriptions"))
    {
      handleGetSensorDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "sensorSettings"))
    {
      handleGetSensorSettings(property, query, name);
    }
    else if (0 == strcmp(name, "sensorStates"))
    {
      handleGetSensorStates(property,query, name);
    }
    else if (0 == strcmp(name, "binaryInputStates"))
    {
      handleGetBinaryInputStates(property, query, name);
    }
    else if (0 == strcmp(name, "name"))
    {
      handleGetName(property, query, name);
    }
    else if (0 == strcmp(name, "model"))
    {
      handleGetModel(property, query, name);
    }
    else if (0 == strcmp(name, "modelGuid"))
    {
      handleGetModelGuid(property, query, name);
    }
    else if (0 == strcmp(name, "vendorGuid"))
    {
      handleGetVersionGuid(property, query, name);
    }
    else if (0 == strcmp(name, "hardwareVersion"))
    {
      handleGetHardwareVersion(property, query, name);
    }
    else if (0 == strcmp(name, "configURL"))
    {
      handleGetConfigURL(property, query, name);
    }
    else if (0 == strcmp(name, "hardwareGuid"))
    {
      handleGetHardwareGuid(property, query, name);
    }
    else if (0 == strcmp(name, "hardwareModelGuid"))
    {
      handleGetHardwareModelGuid(property, query, name);
    }
    else if (0 == strcmp(name, "modelUID"))
    {
      handleGetModelUID(property, query, name);
    }
    else if (0 == strcmp(name, "deviceIcon16"))
    {
      handleDeviceIcon(property, name);
    }
    else if (0 == strcmp(name, "deviceIconName"))
    {
      handleDeviceIconName(property, name);
    }
    else if (0 == strcmp(name, "zoneID"))
    {
      handleDeviceZoneId(property, name);
    }
    //else if (0 == strcmp(name, "vendorId"))
    //{
    //  handleDeviceVendorId(property, name);
    //}
    //else if (0 == strcmp(name, "vendorName"))
    //{
    //  handleDeviceVendorName(property, name);
    //}
    //else if (0 == strcmp(name, "oemGuid"))
    //{
    //  handleDeviceOemGuid(property, name);
    //}
    //else if (0 == strcmp(name, "oemModelGuid"))
    //{
    //  handleDeviceOemModelGuid(property, name);
    //}
    else if (0 == strcmp(name, "modelVersion"))
    {
      handleDeviceModelVersion(property, name);
    }
    //else if (0 == strcmp(name, "displayId"))
    //{
    //  handleDeviceDisplayId(property, name);
    //}
    //else if (0 == strcmp(name, "deviceClass"))
    //{
    //  handleDeviceDeviceClass(property, name);
    //}
    //else if (0 == strcmp(name, "deviceClassVersion"))
    //{
    //  handleDeviceDeviceClassVersion(property, name);
    //}
    //else if (0 == strcmp(name, "deviceStates"))
    //{
    //  handleDeviceDeviceStates(property, name);
    //}
    //else if (0 == strcmp(name, "deviceProperties")) 
    //{
    //  handleDeviceDeviceProperties(property, name);
    //}
    else
    {
      LOG(LOG_WARNING, "IVDSD handleVDSDProperties property unknown: name: %s\n", name);
    }
  }
  free(name);
}

/***********************************************************************//**
  @method :  handleSetVDSDProperties
  @comment:  handle a set property query.
             Overwrite the handle... in your own class, if needed.
  @param  :  handle handle of container
  @param  :  property property to fill data
  @param  :  properties property query
***************************************************************************/
uint8_t IVDSD::handleSetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * properties)
{
  char *name;
  for (size_t i = 0; i < dsvdc_property_get_num_properties(properties); i++) {
    int ret = dsvdc_property_get_name(properties, i, &name);
    if (ret != DSVDC_OK) {
      return DSVDC_ERR_INVALID_PROPERTY;
    }
    if (!name)
    {
      LOG(LOG_DEBUG, "IVDSD:: handleSetVDSDProperties not yet handling wildcard properties\n");
      continue;
    }

    LOG(LOG_DEBUG, "**** property name: %s\n", name);
    if (0 == strcmp(name, "zoneID"))
    {
      uint8_t ret = handleSetZoneID(property, properties, name);
      if (ret != DSVDC_OK) return ret;
    }
    else if (0 == strcmp(name, "buttonInputSettings"))
    {
      uint8_t ret = handleSetButtonInputSettings(property, properties, name);
      if (ret != DSVDC_OK) return ret;
    }
    else if (0 == strcmp(name, "outputSettings"))
    {
      uint8_t ret = handleSetOutputSettings(property, properties, name);
      if (ret != DSVDC_OK) return ret;
    }
    else if (0 == strcmp(name, "name"))
    {
      uint8_t ret = handleSetName(property, properties, name);
      if (ret != DSVDC_OK) return ret;
    }
  }
  return DSVDC_OK;
}

/***********************************************************************//**
  @method :  handleIdentify
  @comment:  handle identify
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleIdentify(int32_t group, int32_t zone_id)
{
  LOG(LOG_DEBUG, "IVDSD handleIdentify not implemented group:%i, zone :%i\n", group, zone_id);
}

/***********************************************************************//**
  @method :  handleSetControl
  @comment:  handle the control callback
  @param  :  value ..
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleSetControl(int32_t value, int32_t group, int32_t zone)
{
  /* nop */
  LOG(LOG_DEBUG, "IVDSD handleSetControl not implemented value: %i, group:%i, zone :%i\n", value, group, zone);
}

/***********************************************************************//**
  @method :  handleSetControl
  @comment:  handle the control callback
  @param  :  value ..
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleSetChannelValue(bool apply, double value, int32_t channel)
{
  /* todo*/
  LOG(LOG_DEBUG, "IVDSD handleSetChannelValue not implemented apply: %i, value: %d, gchannel:%i\n", apply, value, channel);
}

/***********************************************************************//**
  @method :  handleSetCallScene
  @comment:  handle the call scene callback
  @param  :  scene ..
  @param  :  force ..
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone)
{
  LOG(LOG_DEBUG, "IVDSD handleSetCallScene not implemented scene: %i, force: %d, group:%i, zone %i\n", scene, force, group, zone);
}

/***********************************************************************//**
  @method :  handleSetSaveScene
  @comment:  handle the call scene callback
  @param  :  scene ..
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleSetSaveScene(int32_t scene, int32_t group, int32_t zone)
{
  LOG(LOG_DEBUG, "IVDSD handleSetSaveScene not implemented scene: %i, group:%i, zone %i\n", scene, group, zone);
}


/***********************************************************************//**
  @method :  handleSetMincene
  @comment:  handle the call scene callback
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleSetMinScene(int32_t group, int32_t zone)
{
  LOG(LOG_DEBUG, "IVDSD handleSetMinScene not implemented scene: %i, group:%i, zone %i\n", group, zone);
}

/***********************************************************************//**
  @method :  notifyModelChange
  @comment:  notify model has changed
***************************************************************************/
void IVDSD::notifyModelChange()
{
  LOG(LOG_DEBUG, "IVDSD notifyModelChange\n");
  if (m_sModelUpdater) {
    m_sModelUpdater->notifyModelChange();
  }
}

/***********************************************************************//**
  @method :  setModelUpdater
  @comment:  set model updater
  @param  :  updateSubject model updater
***************************************************************************/
void IVDSD::setModelUpdater(ModelSubject* updateSubject)
{
  m_sModelUpdater = updateSubject;
}

/***********************************************************************//**
  @method :  vanish
  @comment:  send vanish device and set state to offline
***************************************************************************/
void IVDSD::vanish()
{
  LOG(LOG_DEBUG, "IVDSD::vanish\n");
  assert(m_VDCAccess);

  if (m_conState != CON_OFFLINE) {
    char dsuidstringVdcd[DSUID_STR_LEN];
    ::dsuid_to_string(&m_dsuid, dsuidstringVdcd);
    dsvdc_device_vanished(m_VDCAccess->getHandle(), dsuidstringVdcd);
  }

  sigOffline();
}

/***********************************************************************//**
  @method :  handleDeviceIcon
  @comment:  handler for device icon
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void IVDSD::handleDeviceIcon(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceIcon\n");
  dsvdc_property_add_bytes(property, name, m_icon, m_iconSize);
}

/***********************************************************************//**
  @method :  handleDeviceIconName
  @comment:  handler for device icon name
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void IVDSD::handleDeviceIconName(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceIconName\n");
  dsvdc_property_add_string(property, name, m_iconName.c_str());
}


/***********************************************************************//**
  @method :  handleDeviceZoneId
  @comment:  handler for device zone id
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void IVDSD::handleDeviceZoneId(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceZoneId\n");
  dsvdc_property_add_uint(property, name, m_zoneID);
}

void IVDSD::handleDeviceVendorId(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceVendorId\n");
  dsvdc_property_add_string(property, name, "intertechno");
}

void IVDSD::handleDeviceVendorName(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceVendorName\n");
  dsvdc_property_add_string(property, name, "intertechno");
}

void IVDSD::handleDeviceOemGuid(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceOemGuid\n");
  dsvdc_property_add_string(property, name, "intertechno");

}

void IVDSD::handleDeviceOemModelGuid(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceOemModelGuid\n");

}

void IVDSD::handleDeviceModelVersion(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceModelVersion\n");
  dsvdc_property_add_string(property, name, "1.0.1");
}

void IVDSD::handleDeviceDisplayId(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceDisplayId\n");
  dsvdc_property_add_string(property, name, "output switch");
}

void IVDSD::handleDeviceDeviceClass(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceDeviceClass\n");
}

void IVDSD::handleDeviceDeviceClassVersion(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceDeviceClassVersion\n");

}

void IVDSD::handleDeviceDeviceStates(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceDeviceStates\n");

}

void IVDSD::handleDeviceDeviceProperties(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceDeviceProperties\n");

}

/***********************************************************************//**
  @method :  handleSetZoneID
  @comment:  handler for device zone id
  @param  :  property fill data
  @param  :  properties list of data
  @param  :  name name of property
***************************************************************************/
uint8_t IVDSD::handleSetZoneID(dsvdc_property_t* /*property*/, const dsvdc_property_t* properties, char* /*name*/) {
  uint64_t tempZoneID;
  dsvdc_property_get_uint(properties,0,&tempZoneID);
  m_zoneID = tempZoneID;
  notifyModelChange();
  return DSVDC_OK;
}

