/***************************************************************************
                           ioqueue.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IOQUEUE_H
#define IOQUEUE_H

#include <stdint.h>
#include <queue>
#include <memory>
#include <condition_variable>
#include <boost/noncopyable.hpp>

struct TransferData {
  int16_t m_address1;
  int16_t m_address2;
  bool m_on;
};

class IOQueue : public boost::noncopyable
{
public:
  void insert(std::shared_ptr<TransferData> data);
  std::shared_ptr<TransferData> get(bool blocking = false);
  bool queueHasData();
  void waitDataReady();

private:
  std::queue<std::shared_ptr<TransferData> > m_OutputQueue;
  std::mutex m_lockOutputQueue;
  std::condition_variable m_cvOutputQueue;
};

#endif // IOQUEUE_H
