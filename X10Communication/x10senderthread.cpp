/***************************************************************************
                           x10senderthread.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "x10senderthread.h"
#include "utils/logger.h"
#include <cstring>
#include <unistd.h>

/***********************************************************************//**
  @method : X10SenderThread
  @comment: constructor
  @param  : queue input queue
  @param  : interface serial line interface
***************************************************************************/
X10SenderThread::X10SenderThread(std::shared_ptr <IOQueue> queue, std::shared_ptr <SerialInterface> interface) :
  m_Queue{queue},
  m_Interface{interface}
{
}

/***********************************************************************//**
  @method : ~X10SenderThread
  @comment: destructor
***************************************************************************/
X10SenderThread::~X10SenderThread()
{
}

/***********************************************************************//**
  @method : run
  @comment: main thread function
***************************************************************************/
void X10SenderThread::run()
{
  while(isRunning()) {
    m_Queue->waitDataReady();
    std::shared_ptr<TransferData>  data = m_Queue->get();
    if (data) {
      sendData(data);
    }
  }
}

/***********************************************************************//**
  @method : sendData
  @comment: send data to serial line
  @param  : data container with data
***************************************************************************/
void X10SenderThread::sendData(std::shared_ptr<TransferData> data)
{
  // ADDRESS A..P
  uint8_t outData [6];

  std::memset(outData, 0, sizeof(outData));
  outData[0] = 'V';

  outData[1] = 0x41; //"A"
  outData[1]+= data->m_address1;

  outData[2] = 0x30; // "0"
  outData[2] += data->m_address2;

  outData[3] = data->m_on ? 0x45 : 0x36; // E or 6

  outData[4] = 0x58; // "X"

  LOG(LOG_DEBUG, "X10SenderThread.::sendData address1: %i, address2: %i, on: %d telegram:%s\n", data->m_address1, data->m_address2, data->m_on, outData);
  m_Interface->sendStr((uint8_t*) outData, 5);
  usleep(150000);
  m_Interface->sendStr((uint8_t*) outData, 5);
  usleep(100000);
}

