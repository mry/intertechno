/***************************************************************************
                           serialline.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SERIALLINE_H
#define SERIALLINE_H

#include "serialinterface.h"
#include <memory>

class SerialLine : public SerialInterface,
                   public std::enable_shared_from_this<SerialLine>
{

public:
  explicit SerialLine(const char *port);
  ~SerialLine();

public:
  bool setDtr(bool bDtrSig) override;
  bool setRts(bool bRtsSig) override;
  bool setBaud(uint32_t lBaud) override;
  bool openPort() override;
  bool closePort() override;
  bool sendStr(uint8_t *data, uint16_t size) override;
  bool readStr(uint8_t *data, uint16_t &len) override;

public:
  enum
  {
    LEN_DEVICE = 32
  };

private:
  uint16_t m_localfd;
  char m_pPort[LEN_DEVICE];
};

#endif // SERIALLINE_H
