/***************************************************************************
                           serialinterface.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include <stdint.h>

class SerialInterface
{

public:
  virtual ~SerialInterface() = default;

public:
  virtual bool setDtr(bool bDtrSig) = 0;
  virtual bool setRts(bool bRtsSig) = 0;
  virtual bool setBaud(uint32_t lBaud) = 0;
  virtual bool openPort() = 0;
  virtual bool closePort() = 0;
  virtual bool sendStr(uint8_t *data, uint16_t size) = 0;
  virtual bool readStr(uint8_t *data, uint16_t &len) = 0;
};

#endif // SERIALINTERFACE_H
