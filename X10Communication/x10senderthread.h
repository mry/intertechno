/***************************************************************************
                           x10senderthread.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef X10SENDERTHREAD_H
#define X10SENDERTHREAD_H

#include <memory>
#include "utils/threadinterface.h"
#include "X10Communication/ioqueue.h"
#include "X10Communication/serialinterface.h"

class X10SenderThread :
    public ThreadInterface
{

public:
  X10SenderThread(std::shared_ptr <IOQueue> queue, std::shared_ptr <SerialInterface> interface);
  ~X10SenderThread();

protected:
  void run() override;

private:
  void sendData(std::shared_ptr<TransferData> data);

private:  
  std::shared_ptr <IOQueue> m_Queue;
  std::shared_ptr <SerialInterface> m_Interface;
};

#endif // X10SENDERTHREAD_H
