/***************************************************************************
                           ioqueue.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ioqueue.h"

/***********************************************************************//**
  @method : insert
  @comment: insert data to queue
  @param  : data
  @return : -
***************************************************************************/
void IOQueue::insert(std::shared_ptr<TransferData> data)
{
  std::unique_lock<std::mutex> lock(m_lockOutputQueue);
  m_OutputQueue.push(data);
  m_cvOutputQueue.notify_one();
}

/***********************************************************************//**
  @method : get
  @comment: get data from queue
  @param  : -
  @return : data can be null
***************************************************************************/
std::shared_ptr<TransferData> IOQueue::get(bool blocking)
{
  if (blocking) {
    waitDataReady();
  }

  std::shared_ptr<TransferData> output;
  std::unique_lock<std::mutex> lock(m_lockOutputQueue);
  if (!m_OutputQueue.empty()) {
    output = m_OutputQueue.front();
    m_OutputQueue.pop();
  }
  return output;
}

/***********************************************************************//**
  @method : queueHasData
  @comment: check if queue has data
  @param  : -
  @return : true has data
***************************************************************************/
bool IOQueue::queueHasData()
{
  std::unique_lock<std::mutex> lock(m_lockOutputQueue);
  return (!m_OutputQueue.empty());
}

/***********************************************************************//**
  @method : waitDataReady
  @comment: wait until data
  @param  : -
  @return : true has data
***************************************************************************/
void IOQueue::waitDataReady()
{
  std::unique_lock<std::mutex> lock(m_lockOutputQueue);
  while (m_OutputQueue.empty()) {  // loop to avoid spurious wakeups
    m_cvOutputQueue.wait(lock);
  }
}
