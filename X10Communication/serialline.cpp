/***************************************************************************
                           serialline.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "serialline.h"
#include "utils/logger.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>

/***********************************************************************//**
  @method : SerialLine
  @comment: constructor
  @param  : port
  @return : -
***************************************************************************/
SerialLine::SerialLine(const char * port)
  : SerialInterface()
{
  ::strncpy(m_pPort, port, sizeof(m_pPort));
  m_localfd = 0;
}

/***********************************************************************//**
  @method : SerialLine
  @comment: destructor
  @param  : -
  @return : -
***************************************************************************/
SerialLine::~SerialLine()
{
  if (m_localfd){
    ::close (m_localfd);
  }
}

/***********************************************************************//**
  @method : setDtr
  @comment: set dtr level
  @param  : port port id
  @param  : bDtrSig signal behaviour
  @return : state
***************************************************************************/
bool SerialLine::setDtr(bool bDtrSig)
{
  bool retVal = false;

  if ( m_localfd ){
    int16_t status;
    struct termios tio;

    ::tcgetattr ( m_localfd, &tio );
    tio.c_cflag &= ~HUPCL;
    ::tcsetattr ( m_localfd, TCSANOW, &tio );
    ::ioctl ( m_localfd, TIOCMGET, &status );

    if ( bDtrSig ){
      status &= ~TIOCM_DTR;
    }else{
      status |=  TIOCM_DTR;
    }
    ::ioctl ( m_localfd, TIOCMSET, &status );
    retVal = true;
  }

  return retVal;
}

/***********************************************************************//**
  @method : setRTS
  @comment: set rts level
  @param  : port port id
  @param  : bRtsSig signal behaviour
  @return : state
***************************************************************************/
bool SerialLine::setRts(bool bRtsSig)
{
  bool retVal = false;
  if (m_localfd){
    int16_t status;
    struct termios tio;
    ::tcgetattr ( m_localfd, &tio );
    tio.c_cflag &= ~HUPCL;
    ::tcsetattr ( m_localfd, TCSANOW, &tio );
    ::ioctl ( m_localfd, TIOCMGET, &status );

    if ( bRtsSig ){
        status &= ~TIOCM_RTS;
      }else{
        status |=  TIOCM_RTS;
      }
    ::ioctl ( m_localfd, TIOCMSET, &status );
    retVal = true;
  }
  return retVal;
}

/***********************************************************************//**
  @method : setBaud
  @comment: set baud rate
  @param  : port port id
  @param  : lBaud baud rate {300,600,1200,9600,19200,38400} default 19200
  @return : state
***************************************************************************/
bool SerialLine::setBaud(uint32_t lBaud)
{
  bool retVal = false;
  uint32_t baud;

  switch ( lBaud ){
  case 300   : baud = B300;   break;
  case 600   : baud = B600;   break;
  case 1200  : baud = B1200;  break;
  case 2400  : baud = B2400;  break;
  case 9600  : baud = B9600;  break;
  case 19200 : baud = B19200; break;
  case 38400 : baud = B38400; break;
  default:     baud= B19200;  break;
  }

  //fd = ::open ( port, O_RDWR | O_NDELAY | O_NOCTTY );
  if (m_localfd){
    struct termios tio;
    ::tcgetattr ( m_localfd, &tio );
    tio.c_cflag  =   baud | CS8 | CREAD| CLOCAL;
    tio.c_cflag &= ~ HUPCL;
    tio.c_lflag  =   0;
    tio.c_iflag  =   IGNPAR;
    tio.c_oflag  =   0;
    tio.c_cc [VTIME] = 1;
    tio.c_cc [VMIN ] = 1; // min 1 character
    ::tcflush   ( m_localfd, TCIFLUSH );
    ::tcsetattr ( m_localfd, TCSANOW, &tio);
    LOG(LOG_INFO, "baudrate set.");
    retVal = true;
  }else{
    LOG(LOG_ERR,"baudrate not set");
  }
  return retVal;
}

/***********************************************************************//**
  @method : openPort
  @comment: open a communication port
  @param  : port port id
  @return : state
***************************************************************************/
bool SerialLine::openPort()
{
  bool retVal = false;
  m_localfd = ::open ( m_pPort, O_RDWR);

  if (m_localfd){
    LOG(LOG_DEBUG, "port opened: %s", m_pPort);
    retVal = true;
  }else{
    LOG(LOG_ERR, "port not opened");
  }
  return retVal;
}

/***********************************************************************//**
  @method : closePort
  @comment: close a previously opened port
  @param  : port port id
  @return : state
***************************************************************************/
bool SerialLine::closePort()
{
  bool retVal = false;
  if(m_localfd){
    ::close (m_localfd);
    m_localfd = 0;
    LOG( LOG_INFO, "port closed");
    retVal = true;
  }else{
    LOG(LOG_ERR, "port not closed");
  }
  return retVal;
}

/***********************************************************************//**
  @method : sendStr
  @comment: send data
  @param  : port port id
  @param  : data string to send. MUST BE TERMINATED WITH \0
  @return : state
***************************************************************************/
bool SerialLine::sendStr(uint8_t* data, uint16_t size)
{
  bool retVal = false;
  if (m_localfd){
    retVal = ::write (m_localfd, data, size);
    //LOG(LOG_DEBUG, (char*)data);
  }
  return retVal;
}

/***********************************************************************//**
  @method : readStr
  @comment: read data
  @param  : port port id
  @param  : data pointer of buffer
  @param  : length of buffer in bytes (in/out)
  @return : state
***************************************************************************/
bool SerialLine::readStr(uint8_t* data, uint16_t& len)
{
  ssize_t retVal = 0;
  if (m_localfd){
    retVal = ::read (m_localfd, data, len);
    if (retVal >= 0) {
      len = retVal;
    } else {
      len = 0;
    }
  }
  return (retVal > 0);
}
