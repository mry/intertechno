/***************************************************************************
                           threadinterface.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef THREADINTERFACE_H
#define THREADINTERFACE_H

#include <pthread.h>

/// forward declaration to setup friend.
void * doRun(void *ptr);

class ThreadInterface
{

public:
  ThreadInterface();
  virtual ~ThreadInterface();
  virtual void startThread();
  virtual void stopThread();
  friend void * doRun(void *ptr);

  bool isRunning() const {return m_running;}

protected:
  /***********************************************************************//**
    @method : run
    @comment: main threading operation.
              Has to be implemented in subclass
  ***************************************************************************/
  virtual void run() = 0;

private:
  bool m_running;
  pthread_t m_thread;
};

#endif // THREADINTERFACE_H
