/***************************************************************************
                           converter.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONVERTER_H
#define CONVERTER_H

#include <string>
#include <vector>

namespace converter
{
  std::string trim(const std::string& _str);

  int strToInt(const std::string& _strValue);

  unsigned int strToUInt(const std::string& _strValue);
  int strToIntDef(const std::string& _strValue, const int _default);
  unsigned int strToUIntDef(const std::string& _strValue, const unsigned int _default);
  unsigned long long strToULongLong(const std::string& _strValue);
  unsigned long long strToULongLongDef(const std::string& _strValue, const unsigned int _default);

  std::string intToString(const long long, const bool _hex = false);
  std::string uintToString(const long long unsigned, bool _hex = false);

  double strToDouble(const std::string& _strValue);
  double strToDoubleDef(const std::string& _strValue, const double _default);

  std::string unsignedLongIntToHexString(const unsigned long long _value);
  std::string unsignedLongIntToString(const unsigned long long _value);

  std::string doubleToString(const double _value);

  std::vector<std::string> splitString(const std::string& _source,
                                       const char _delimiter,
                                       bool _trimEntries);
};

#endif // CONVERTER_H
