/***************************************************************************
                           threadinterface.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "threadinterface.h"

/***********************************************************************//**
  @method :  doRun
  @comment:  a function passed to pthread_create.
             Assumption: the fourth argument of pthread_create is
             the this pointer of the MainThread.
***************************************************************************/
void * doRun(void *ptr)
{
  ThreadInterface* pThread = reinterpret_cast<ThreadInterface*>(ptr);
  pThread->run();
  return 0;
}

/***********************************************************************//**
  @method :  ThreadInterface
  @comment:  constructor
***************************************************************************/
ThreadInterface::ThreadInterface() :
  m_running(false)
{
}

/***********************************************************************//**
  @method :  ~ThreadInterface
  @comment:  destructor
***************************************************************************/
ThreadInterface::~ThreadInterface()
{
}

/***********************************************************************//**
  @method :  startThread
  @comment:  start the posix thread.
***************************************************************************/
void ThreadInterface::startThread()
{
  if (!m_running) {
    m_running = (0 == pthread_create( &m_thread, NULL, doRun, this));
  }
}

/***********************************************************************//**
  @method :  stopThread
  @comment:  stop the posix thread.
***************************************************************************/
void ThreadInterface::stopThread()
{
  if (m_running) {
    m_running = false;
    pthread_join(m_thread, NULL);
  }
}
