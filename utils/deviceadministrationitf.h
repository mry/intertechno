/***************************************************************************
                           persistencyitf.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DEVICEADMINISTRATIONITF
#define DEVICEADMINISTRATIONITF

#include <digitalSTROM/dsuid.h>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

class IVDSD;

class DeviceAdministrationItf
{
public:
  virtual ~DeviceAdministrationItf(){};
  virtual void createX10OutputDevice(int32_t address1, int32_t address2) = 0;
  virtual void removeX10OutputDevice(dsuid_t& dsuid) = 0;
  virtual std::vector<boost::shared_ptr<IVDSD> > getDevices() = 0;
};

#endif // DEVICEADMINISTRATIONITF

