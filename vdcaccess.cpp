/***************************************************************************
                           vdcaccess.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <vector>

#include <algorithm>
#include <boost/bind.hpp>
#include <digitalSTROM/dsuid.h>

#include "vdcaccess.h"
#include "utils/logger.h"

namespace // anonymous namespace
{

/***********************************************************************//**
  @method :  newsession_cb
  @comment:  callback new session
  @param  :  handle input
  @param  :  userdata input
***************************************************************************/
static void newsession_cb(dsvdc_t* handle, void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->newSession(handle);
}

/***********************************************************************//**
  @method :  ping_cb
  @comment:  callback ping
  @param  :  handle input
  @param  :  dsuid input
  @param  :  userdata input
***************************************************************************/
static void ping_cb(dsvdc_t* handle , const char* dsuid,  void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->ping(handle, dsuid);
}

/***********************************************************************//**
  @method :  setidentify_cb
  @comment:  callback identify device
  @param  :  handle input
  @param  :  dsuid input
  @param  :  n_dsuid number of dsuids
  @param  :  group
  @param  :  zoneId
  @param  :  user input
***************************************************************************/
void setidentify_cb(dsvdc_t *handle, char **dsuid,
                    size_t n_dsuid, int32_t* group,
                    int32_t* zone_id, void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  int32_t outGroup = 0;
  if (group) {outGroup = *group;}

  int32_t outZone = 0;
  if (zone_id) {outZone =*zone_id;}

  wrapper->setidentify(handle, dsuid, n_dsuid, outGroup, outZone);
}

/***********************************************************************//**
  @method :  endsession_cb
  @comment:  callback end session
  @param  :  handle input
  @param  :  userdata input
***************************************************************************/
static void endsession_cb(dsvdc_t* handle, void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->endSession(handle);
}

/***********************************************************************//**
  @method :  getprop_cb
  @comment:  get property callback
  @param  :  handle input
  @param  :  dsuid input
  @param  :  property output
  @param  :  query input
  @param  :  userdata input
***************************************************************************/
static void getprop_cb(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* query,  void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->getprop(handle, dsuid, property, query);
}

/***********************************************************************//**
  @method :  setprop_cb
  @comment:  set property callback
  @param  :  handle input
  @param  :  dsuid input
  @param  :  property output
  @param  :  properties input
  @param  :  userdata input
***************************************************************************/
static void setprop_cb (dsvdc_t *handle, const char *dsuid, dsvdc_property_t *property, const dsvdc_property_t *properties, void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->setprop(handle, dsuid, property, properties);
}

/***********************************************************************//**
  @method :  setcontrol_cb
  @comment:  set control value callback
  @param  :  handle input
  @param  :  dsuid inputs (array)
  @param  :  n_dsuid number of dsuid entries in inputs
  @param  :  value value to set
  @param  :  group
  @param  :  zone_id
  @param  :  name
  @param  :  userdata input
***************************************************************************/
static void setcontrol_cb(dsvdc_t *handle, char **dsuid, size_t n_dsuid,
                 double value, int32_t* group, int32_t* zone_id,
                 char* name, void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  LOG(LOG_INFO, "VDCAccess::setControl_cb\n");

  int32_t outGroup = 0;
  if (group) {outGroup = *group;}

  int32_t outZone = 0;
  if (zone_id) {outZone =*zone_id;}

  for (size_t n = 0; n < n_dsuid; ++ n) {
    LOG(LOG_INFO, "VDCAccess::setControl_cb:dsuid: %s\n", dsuid[n]);
    wrapper->setcontrol(handle, dsuid[n], value, outGroup, outZone);
  }
}

/***********************************************************************//**
  @method :  setchannelvalue_cb
  @comment:  set channel value callback
  @param  :  handle input
  @param  :  dsuid inputs (array)
  @param  :  n_dsuid number of dsuid entries in inputs
  @param  :  channel
  @param  :  channelid
  @param  :  value
  @param  :  userdata input
***************************************************************************/
static void setchannelvalue_cb(dsvdc_t *handle, char **dsuid, size_t n_dsuid,
                               int32_t channel, const char* channelid,
                               bool *apply, double *value, void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  LOG(LOG_INFO, "VDCAccess::setchannelvalue_cb\n");

  for (size_t n = 0; n < n_dsuid; ++ n) {
    LOG(LOG_INFO, "VDCAccess::setchannelvalue_cb:dsuid: %s\n", dsuid[n]);
    wrapper->setChannelValue(handle, dsuid[n], *apply, *value, channel);
  }
}

/***********************************************************************//**
  @method :  setcallscene_cb
  @comment:  set call scene
  @param  :  handle input
  @param  :  dsuid inputs (array)
  @param  :  n_dsuid number of dsuid entries in inputs
  @param  :  scene
  @param  :  force
  @param  :  group
  @param  :  zone_id
  @param  :  userdata input
***************************************************************************/
static void setcallscene_cb (dsvdc_t *handle, char **dsuid, size_t n_dsuid,
                             int32_t scene, bool force, int32_t* group,
                             int32_t* zone_id, void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  LOG(LOG_INFO, "VDCAccess::setcallscene_cb\n");

  int32_t outGroup = 0;
  if (group) {outGroup = *group;}

  int32_t outZone = 0;
  if (zone_id) {outZone =*zone_id;}

  for (size_t n = 0; n < n_dsuid; ++ n) {
    LOG(LOG_INFO, "VDCAccess::setcallscene_cb:dsuid: %s\n", dsuid[n]);
    wrapper->setCallScene(handle, dsuid[n], scene, force, outGroup, outZone);
  }
}

/***********************************************************************//**
  @method :  setsavescene_cb
  @comment:  save scene
  @param  :  handle input
  @param  :  dsuid inputs (array)
  @param  :  n_dsuid number of dsuid entries in inputs
  @param  :  scene
  @param  :  group
  @param  :  zone_id
  @param  :  userdata input
***************************************************************************/
static void setsavescene_cb (dsvdc_t *handle, char **dsuid, size_t n_dsuid,
                             int32_t scene, int32_t* group, int32_t* zone_id,
                             void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  LOG(LOG_INFO, "VDCAccess::setsavescene_cb\n");

  int32_t outGroup = 0;
  if (group) {outGroup = *group;}

  int32_t outZone = 0;
  if (zone_id) {outZone =*zone_id;}

  for (size_t n = 0; n < n_dsuid; ++ n) {
    LOG(LOG_INFO, "VDCAccess::setsavescene_cb:dsuid: %s\n", dsuid[n]);
    wrapper->setSaveScene(handle, dsuid[n], scene, outGroup, outZone);
  }
}

/***********************************************************************//**
  @method :  setmincallscene_cb
  @comment:  set min call scene (i.e. off)
  @param  :  handle input
  @param  :  dsuid inputs (array)
  @param  :  n_dsuid number of dsuid entries in inputs
  @param  :  group
  @param  :  zone_id
  @param  :  userdata input
***************************************************************************/
static void setmincallscene_cb(dsvdc_t *handle, char **dsuid, size_t n_dsuid,
                 int32_t* group, int32_t* zone_id, void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  LOG(LOG_INFO, "VDCAccess::setmincallscene_cb\n");

  int32_t outGroup = 0;
  if (group) {outGroup = *group;}

  int32_t outZone = 0;
  if (zone_id) {outZone =*zone_id;}

  for (size_t n = 0; n < n_dsuid; ++ n) {
    LOG(LOG_INFO, "VDCAccess::setmincallscene_cb:dsuid: %s\n", dsuid[n]);
    wrapper->setMinScene(handle, dsuid[n], outGroup, outZone);
  }
}

/***********************************************************************//**
  @method :  announce_container_cb
  @comment:  announce container
  @param  :  handle input
  @param  :  code
  @param  :  userdata input
***************************************************************************/
static void announce_container_cb(dsvdc_t *handle, int code, void *arg,
                               void *userdata)
{
    (void)handle;
    (void)userdata;
    LOG(LOG_INFO, "VDCAccess::announce_container_cb %i\n", code);
}

} // end anonymous namespace

/***********************************************************************//**
  @method :  VDCAccess
  @comment:  constructor
***************************************************************************/
VDCAccess::VDCAccess() :
  m_handle{0},
  m_ready{false}
{
}

/***********************************************************************//**
  @method :  registerVDC
  @comment:  register the vdc (only one item)
  @param  :  vdc input
***************************************************************************/
void VDCAccess::registerVDC(boost::shared_ptr<IVDC> vdc)
{
  m_vdc = vdc;
}

/***********************************************************************//**
  @method :  getVDC
  @comment:  get the registered vdc
  @return :  vdc
***************************************************************************/
boost::shared_ptr<IVDC> VDCAccess::getVDC()
{
  return m_vdc;
}

/***********************************************************************//**
  @method :  registerVDSD
  @comment:  register a vdsd
  @param  :  vdsd input
***************************************************************************/
void VDCAccess::registerVDSD(boost::shared_ptr<IVDSD> vdsd)
{
  const dsuid_t tempDsuid = vdsd->getDsuid();

  char dsuidstringVdsd[DSUID_STR_LEN];
  ::dsuid_to_string(&tempDsuid, dsuidstringVdsd);

  std::unique_lock<std::mutex> lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter == m_vdsd.end()) {
    m_vdsd.insert( std::pair<dsuid_t, boost::shared_ptr<IVDSD> > (tempDsuid, vdsd));
    LOG(LOG_INFO, "VDCAccess::registerVDSD: dsuid: %s\n", dsuidstringVdsd);
    vdsd->modelUpdate();
  }
}

/***********************************************************************//**
  @method :  unregisterVDSD
  @comment:  unregister a vdsd
  @param  :  vdsdDsuid dsuid key of device to remove
***************************************************************************/
void VDCAccess::unregisterVDSD(dsuid_t& vdsdDsuid)
{
  std::unique_lock<std::mutex> lock(m_AccessMutex);

  VDSD_CONST_ITERATOR iter = m_vdsd.find(vdsdDsuid);
  if (iter != m_vdsd.end()) {
    char dsuidstringVdsd[DSUID_STR_LEN];
    ::dsuid_to_string(&vdsdDsuid, dsuidstringVdsd);
    dsvdc_device_vanished(m_handle, dsuidstringVdsd);
    iter->second->modelUpdate();
    m_vdsd.erase(vdsdDsuid);
    LOG(LOG_INFO, "VDCAccess::unregisterVDSD: dsuid: %s\n", dsuidstringVdsd);
  }
}

/***********************************************************************//**
  @method :  getDevices
  @comment:  get a list of vdc devices
  @return :  vector of ivdsd
***************************************************************************/
std::vector<boost::shared_ptr<IVDSD> > VDCAccess::getDevices()
{
  std::vector<boost::shared_ptr<IVDSD> >  vec;
  std::transform(m_vdsd.begin(),
                 m_vdsd.end(),
                 std::back_inserter(vec),
                 boost::bind(&VDSD_COLLECTION::value_type::second,_1));
  return vec;
}

/***********************************************************************//**
  @method :  getHandle
  @comment:  get the handle of the VDCAccess instance
  @return :  handle
***************************************************************************/
dsvdc_t* VDCAccess::getHandle() const
{
  return m_handle;
}

/***********************************************************************//**
  @method :  initializeLib
  @comment:  initialize the library and set the handle
  @return :  true initialized
***************************************************************************/
bool VDCAccess::initializeLib()
{
  if (!m_vdc) {
    return false;
  }

  const dsuid_t tempDsuid = m_vdc->getDsuid();
  char dsuidstring[34];
  ::dsuid_to_string(&tempDsuid, dsuidstring);

  if (dsvdc_new(0, dsuidstring, m_vdc->getName().c_str(), false, this, &m_handle) != DSVDC_OK) {
    return false;
  }
  return true;
}

/***********************************************************************//**
  @method :  registerCallbacks
  @comment:  register the callbacks in the library
***************************************************************************/
void VDCAccess::registerCallbacks()
{
  /* setup callbacks */
  dsvdc_set_new_session_callback(m_handle, newsession_cb);
  dsvdc_set_end_session_callback(m_handle, endsession_cb);

  dsvdc_set_ping_callback(m_handle, ping_cb);
  dsvdc_set_identify_notification_callback(m_handle,setidentify_cb);
  dsvdc_set_get_property_callback(m_handle, getprop_cb);
  dsvdc_set_set_property_callback(m_handle, setprop_cb);
  dsvdc_set_control_value_callback(m_handle, setcontrol_cb);
  dsvdc_set_output_channel_value_callback(m_handle, setchannelvalue_cb);
  dsvdc_set_call_scene_notification_callback(m_handle, setcallscene_cb);
  dsvdc_set_call_min_scene_notification_callback(m_handle, setmincallscene_cb);
  dsvdc_set_save_scene_notification_callback(m_handle, setsavescene_cb);
}

/***********************************************************************//**
  @method :  unregisterCallbacks
  @comment:  unregister the callbacks in the library,
             which were previously registered.
***************************************************************************/
void VDCAccess::unregisterCallbacks()
{
  dsvdc_set_new_session_callback(m_handle, NULL);
  dsvdc_set_end_session_callback(m_handle, NULL);

  dsvdc_set_identify_notification_callback(m_handle,NULL);
  dsvdc_set_get_property_callback(m_handle, NULL);
  dsvdc_set_set_property_callback(m_handle, NULL);
  dsvdc_set_control_value_callback(m_handle, NULL);
  dsvdc_set_output_channel_value_callback(m_handle, NULL);
  dsvdc_set_call_scene_notification_callback(m_handle, NULL);
  dsvdc_set_call_min_scene_notification_callback(m_handle, NULL);
  dsvdc_set_save_scene_notification_callback(m_handle, NULL);
}

/***********************************************************************//**
  @method :  newSession
  @comment:  callback
  @param  :  handle input
***************************************************************************/
void VDCAccess::newSession(dsvdc_t *handle)
{
  LOG(LOG_INFO, "VDCAccess::newSession");  
  m_ready = true;

  dsuid_t id = m_vdc->getDsuid();
  char dsuidstringVdsd[DSUID_STR_LEN];
  ::dsuid_to_string(&id, dsuidstringVdsd);

  int ret;
  ret = dsvdc_announce_container(handle,
                                 dsuidstringVdsd,
                                 (void *) dsuidstringVdsd,
                                 announce_container_cb);
  if (ret != DSVDC_OK)
  {
      LOG(LOG_ERR, "VDCAccess::sessionnew: error code: %i\n" , ret );
      return;
  }

#if 0
  ret = dsvdc_announce_device(handle,
                              g_vdc_dsuid,
                              g_dev_dsuid,
                              (void *) g_dev_dsuid,
                              announce_device_cb);
  if (ret != DSVDC_OK)
  {
      printf("dsvdc_announce_device returned error %d\n", ret);
      return;
  }
#endif
}

/***********************************************************************//**
  @method :  endSession
  @comment:  callback
  @param  :  handle input
***************************************************************************/
void VDCAccess::endSession(dsvdc_t */*handle*/)
{
  LOG(LOG_INFO, "VDCAccess::end session \n");
  m_ready = false;
  resetConnectionState();
}

/***********************************************************************//**
  @method :  ping
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
***************************************************************************/
void VDCAccess::ping(dsvdc_t* handle , const char* dsuid)
{
  LOG(LOG_INFO,"ping %s\n", dsuid);

  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);

  if (m_vdc && m_vdc->isEqual(tempDsuid)) {
    dsvdc_send_pong(handle, dsuid);
    return;
  }

  std::unique_lock<std::mutex> lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter!= m_vdsd.end()) {
    dsvdc_send_pong(handle, dsuid);
  }
}

/***********************************************************************//**
  @method :  setidentify
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd (array)
  @param  :  n_dsuid number of elements in array
  @param  :  group
  @param  :  zone_id
***************************************************************************/
void VDCAccess::setidentify(dsvdc_t */*handle*/, char **dsuid,
                            size_t n_dsuid, int32_t group, int32_t zone_id)
{
  LOG(LOG_INFO, "VDCAccess::setidentify\n");
  for (size_t index = 0; index < n_dsuid; ++index) {
    dsuid_t tempDsuid;
    dsuid_from_string(dsuid[index], &tempDsuid);
    if (m_vdc && m_vdc->isEqual(tempDsuid)) {
      m_vdc->handleIdentify(group,zone_id);
    } else {
      std::unique_lock<std::mutex> lock(m_AccessMutex);
      VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
      if (iter != m_vdsd.end()) {
        boost::shared_ptr<IVDSD> temp = iter->second;
        temp->handleIdentify(group, zone_id);
      }
    }
  }
}

/***********************************************************************//**
  @method :  getprop
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  property output
  @param  :  query input
***************************************************************************/
void VDCAccess::getprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* query)
{
  LOG(LOG_INFO, "VDCAccess::getprop %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);

  if (m_vdc && m_vdc->isEqual(tempDsuid)) {
    m_vdc->handleGetVDCProperties(property, query);
  } else {
    std::unique_lock<std::mutex> lock(m_AccessMutex);
    VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
    if (iter != m_vdsd.end()) {
      boost::shared_ptr<IVDSD> temp = iter->second;
      temp->handleGetVDSDProperties(property, query);
    }
  }

  dsvdc_send_get_property_response(handle, property);
}

/***********************************************************************//**
  @method :  setprop
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  property input
  @param  :  properties input
***************************************************************************/
void VDCAccess::setprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* properties)
{
  LOG(LOG_INFO, "VDCAccess::setprop %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);

  uint8_t code = DSVDC_ERR_NOT_IMPLEMENTED;

  if (m_vdc && m_vdc->isEqual(tempDsuid)) {
    code = m_vdc->handleSetVDCProperties(property, properties);
  } else {
    std::unique_lock<std::mutex> lock(m_AccessMutex);
    VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
    if (iter != m_vdsd.end()) {
      boost::shared_ptr<IVDSD> temp = iter->second;
      code = temp->handleSetVDSDProperties(property, properties);
    }
  }
  dsvdc_send_set_property_response(handle, property, code);
}

/***********************************************************************//**
  @method :  setcontrol
  @comment:  set control value to device with group and zone
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  value
  @param  :  group
  @param  :  zone
***************************************************************************/
void VDCAccess::setcontrol(dsvdc_t* /*handle*/, const char* dsuid, int32_t value, int32_t group, int32_t zone)
{
  LOG(LOG_INFO, "VDCAccess::setcontrol %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);
  std::unique_lock<std::mutex> lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter != m_vdsd.end()) {
    boost::shared_ptr<IVDSD> temp = iter->second;
    temp->handleSetControl(value, group, zone);
  }
}

/***********************************************************************//**
  @method :  setCallScene
  @comment:  set Call scene
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  scene
  @param  :  force
  @param  :  group
  @param  :  zone
***************************************************************************/
void VDCAccess::setCallScene(dsvdc_t* /*handle*/, const char* dsuid, int32_t scene, bool force, int32_t group, int32_t zone)
{
  LOG(LOG_INFO, "VDCAccess::setCallScene %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);
  std::unique_lock<std::mutex> lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter != m_vdsd.end()) {
    boost::shared_ptr<IVDSD> temp = iter->second;
    temp->handleSetCallScene(scene, force, group, zone);
  }
}

/***********************************************************************//**
  @method :  setSaveScene
  @comment:  save scene value
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  scene
  @param  :  group
  @param  :  zone
***************************************************************************/
void VDCAccess::setSaveScene(dsvdc_t* /*handle*/, const char*  dsuid, int32_t scene, int32_t  group, int32_t  zone)
{
  LOG(LOG_INFO, "VDCAccess::setSaveScene %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);
  std::unique_lock<std::mutex> lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter != m_vdsd.end()) {
    boost::shared_ptr<IVDSD> temp = iter->second;
    temp->handleSetSaveScene(scene, group, zone);
  }
}

/***********************************************************************//**
  @method :  setMinScene
  @comment:  set min scene (i.e. off)
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  group
  @param  :  zone
***************************************************************************/
void VDCAccess::setMinScene(dsvdc_t* /*handle*/, const char*  dsuid, int32_t group, int32_t zone)
{
  LOG(LOG_INFO, "VDCAccess::setMinScene %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);
  std::unique_lock<std::mutex> lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter != m_vdsd.end()) {
    boost::shared_ptr<IVDSD> temp = iter->second;
    temp->handleSetMinScene(group, zone);
  }
}

/***********************************************************************//**
  @method :  setChannelValue
  @comment:  set channel value to device
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  apply apply value immeadiately
  @param  :  value
  @param  :  channel
***************************************************************************/
void VDCAccess::setChannelValue(dsvdc_t* /*handle*/, const char* dsuid, bool apply, double value, int32_t channel)
{
  LOG(LOG_INFO, "VDCAccess::setchannelvalue dsuid:%s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);
  std::unique_lock<std::mutex> lock(m_AccessMutex);

  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter != m_vdsd.end()) {
    boost::shared_ptr<IVDSD> temp = iter->second;
    // TODO
    temp->handleSetChannelValue(apply, value, channel);
  }
}


/***********************************************************************//**
  @method :  resetConnectionState
  @comment:  reset the connection state to not connected
***************************************************************************/
void VDCAccess::resetConnectionState()
{
  LOG(LOG_INFO, "VDCAccess::resetConnectionState\n");

  if (m_vdc) {
    m_vdc->resetAnnounced();
  }

  std::unique_lock<std::mutex> lock(m_AccessMutex);
  for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter!= m_vdsd.end(); ++iter) {
    (iter->second)->sigOffline();
  }
}

/***********************************************************************//**
  @method :  run
  @comment:  this operation must be cyclically called
***************************************************************************/
void VDCAccess::run()
{
  /* let the work function do our timing, 1secs timeout */
  dsvdc_work(m_handle, 1);

  if (m_ready) {
    if (m_vdc) {
      m_vdc->checkAnnounce(m_handle);
    }

    processAnnouncement();
    if (m_vdc) {
      std::unique_lock<std::mutex> lock(m_AccessMutex);
      for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
        boost::shared_ptr<IVDSD> temp = (iter->second);
        temp->checkAnnounce();
      }
    }
  }
}

/***********************************************************************//**
  @method :  processAnnouncement
  @comment:  process announcement of devices. Only one device announced
***************************************************************************/
void VDCAccess::processAnnouncement()
{
  if (m_vdc) {
    std::unique_lock<std::mutex> lock(m_AccessMutex);
    // check if any device in state announcing
    bool m_Announcing = false;
    for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
      boost::shared_ptr<IVDSD> temp = (iter->second);
      if (IVDSD::CON_ANNOUNCING == temp->getConnectionState()) {
        m_Announcing = true;
        break;
      }
    }

    // check if device offline
    if (!m_Announcing) {
      for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
        boost::shared_ptr<IVDSD> temp = (iter->second);
        if (IVDSD::CON_OFFLINE == temp->getConnectionState()) {
          temp->sigAnnounce();

          dsuid_t tmpDsuid = temp->getDsuid();
          char dsuidstringVdcd[DSUID_STR_LEN];
          ::dsuid_to_string(&tmpDsuid, dsuidstringVdcd);
          LOG(LOG_INFO, "SEND ANNOUNCE :%s\n", dsuidstringVdcd);
          break;
        }
      }
    }
  }
}


/***********************************************************************//**
  @method :  shutdown
  @comment:  shutdown system. unregister all devices
***************************************************************************/
void VDCAccess::shutdown()
{
  LOG(LOG_DEBUG, "VDCAccess::shutdown\n");
  if (m_vdc) {
    std::unique_lock<std::mutex> lock(m_AccessMutex);
    for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
      boost::shared_ptr<IVDSD> temp = (iter->second);
      temp->vanish();
      temp.reset();
    }
  }
  dsvdc_cleanup(m_handle);
  m_handle = 0;
}

