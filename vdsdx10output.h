/***************************************************************************
                           VDSDX10Output.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDSDX10OUTPUT_H
#define VDSDX10OUTPUT_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <memory>
#include "ivdsd.h"

class IOQueue;

class VDSDX10Output :
    public IVDSD,
    public boost::noncopyable
{
  //---------------------------------------------------------------------------
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int /*version*/)
  {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(IVDSD);
    ar & m_address1;
    ar & m_address2;

    for(int i = 0; i < MAX_SCENES; ++i) {
      ar & m_value[i];
    }

    ar & m_name;
    ar & m_function;
    ar & m_group;
    ar & m_mode;

    for(int i = 0; i < MAX_GROUPS; ++i) {
      ar & m_groupMember[i];
    }
  }
  //---------------------------------------------------------------------------

public:
  VDSDX10Output();
  VDSDX10Output(int32_t address1, int32_t address2);
  ~VDSDX10Output();
  static void setQueue(IOQueue* queue);
  static void setGlobalIcon(const uint8_t* icon16png, size_t size, std::string &iconName);

  double getValue(int channel) const;
  bool getApply() const;

  void process() override;

  void handleSetControl(int32_t value, int32_t group, int32_t zone) override;
  void handleSetChannelValue(bool apply, double value, int32_t channel) override;

  void handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone) override;
  void handleSetSaveScene(int32_t scene, int32_t group, int32_t zone) override;
  void handleSetMinScene(int32_t group, int32_t zone) override;
  void handleIdentify(int32_t group, int32_t zone_id) override;

  int32_t getAddress1()const {return m_address1;};
  int32_t getAddress2()const {return m_address2;};
  std::string getName()const {return m_name;};

protected:
  // get property
  void handleGetPrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetConfigURL(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetModelUID(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetName(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetModel(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;

  void handleGetOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * query, char* name) override;
  void handleGetChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetChannelSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;
  void handleGetChannelStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name) override;

  // set  property
  uint8_t handleSetButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name) override;
  uint8_t handleSetOutputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name) override;
  uint8_t handleSetName(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name) override;

protected:
  void handleDeviceIcon(dsvdc_property_t *property, char* name) override;
  void handleDeviceIconName(dsvdc_property_t *property, char* name) override;

private:
  void emitValue(double value);

  void initialize();

private:
  enum {
    MAX_SCENES = 70,
    MAX_GROUPS = 64
  };

  bool m_apply;
  double m_tempValue;
  int32_t m_scene;
  static IOQueue* m_queue;

  static uint8_t* m_icon;
  static size_t m_iconSize;
  static std::string m_iconName;

  // persistent storage
  int32_t m_address1;
  int32_t m_address2;
  double m_value[MAX_SCENES];
  std::string m_name;
  uint64_t m_function;
  uint64_t m_group;
  uint64_t m_mode;
  bool m_groupMember[MAX_GROUPS];
};

#endif // VDSDX10OUTPUT_H
